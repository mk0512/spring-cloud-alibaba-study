package com.mk.feign;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author makai
 * @Description: TODO
 * @Date 2023/12/15 14:52
 */
@RestController
public class FeignTestController {
    @Resource
    private DemoFeign demoFeign;

    /**
     * 浏览器请求的接口
     * @return
     */
    @GetMapping("/feign")
    public String feign(){
        return demoFeign.get();
    }

    /**
     * feign接口的实现
     * @return
     */
    @GetMapping("/echo")
    public String echo(){
        return "我是打印出來的内容！";
    }
}
