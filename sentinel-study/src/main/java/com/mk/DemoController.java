package com.mk;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.mk.feign.DemoFeign;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author makai
 * @Description: TODO
 * @Date 2023/12/6 16:22
 */
@RestController
public class DemoController {
    @SentinelResource(value = "resource-A",blockHandler = "blockHandler",fallback = "fallBackHandler")
    @GetMapping("/getMsg")
    public String getMsg(){
        return "访问成功";
    }


    /**
     * 限流后处理方法
     * @param e
     * @return
     */
    public String blockHandler(Throwable e){
        return "接口被限流了~~";
    }

    /**
     * 降级后处理方法
     * @param e
     * @return
     */
    public String fallBackHandler(Throwable e){
        return "接口被降级了~~";
    }
}
