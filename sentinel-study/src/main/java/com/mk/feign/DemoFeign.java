package com.mk.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @Author makai
 * @Description: TODO
 * @Date 2023/12/15 14:47
 */
@FeignClient(name = "sentinel-study")
public interface DemoFeign {
    @GetMapping("/echo")
    public String get();
}
