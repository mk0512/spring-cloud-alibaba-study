package com.mk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @Author makai
 * @Description: TODO
 * @Date 2023/12/6 16:23
 */
@EnableFeignClients
@SpringBootApplication
public class WebStarter {
    public static void main(String[] args) {
        SpringApplication.run(WebStarter.class,args);
    }
}
